<?php header("Content-Type: text/html; charset=utf-8");?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <?php include "parts/header.php";?>
</head>
<body>
    <div class="main-wrap">
        <div class="main-wrap__content">
            <?php include "parts/header-layout.php"; ?>

            <section class="page-content-block">
                <div class="container">
                    <div class="back-link-wrap">
                        <a class="back-link" href="#">
                            <svg width="40" height="40">
                                <use xlink:href="images/icon-collections/other-icons.svg#back-arrow-icon"></use>
                            </svg>
                        </a>
                    </div>
                    <div class="page-content sticky-container" data-sticky-container="">

                        <div class="short-container">
                            <h1 class="text-heading-2 page-content-title">CLOUD 53</h1>
                            <div class="uni-text-content">
                                <p>
                                    Автоматизированное тестирование виртуальных вычислительных ресурсов
                                    информационной инфраструктуры, размещенных на платформах публичных и частных облачных
                                    провайдеров, на соответствие политикам автоматического масштабирования.
                                </p>

                            </div>


                            <div class="checkers-block">
                                <p class="title text-heading-4">Область применения</p>

                                <div class="checkers-wrap">
                                    <div class="checkers-box">
                                        <div class="checkers-item">
                                            <div class="checkers-unit">
                                                <div class="checkers-unit__icon">
                                                    <svg width="24" height="24">
                                                        <use xlink:href="images/icon-collections/other-icons.svg#coin-icon"></use>
                                                    </svg>
                                                </div>
                                                <div class="checkers-unit__content text-1">
                                                    Аудит информационной инфраструктуры
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checkers-item">
                                            <div class="checkers-unit">
                                                <div class="checkers-unit__icon">
                                                    <svg width="24" height="24">
                                                        <use xlink:href="images/icon-collections/other-icons.svg#star-icon"></use>
                                                    </svg>
                                                </div>
                                                <div class="checkers-unit__content text-1">
                                                    Оценка квалификации специалистов
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checkers-item">
                                            <div class="checkers-unit">
                                                <div class="checkers-unit__icon">
                                                    <svg width="24" height="24">
                                                        <use xlink:href="images/icon-collections/other-icons.svg#flash-icon"></use>
                                                    </svg>
                                                </div>
                                                <div class="checkers-unit__content text-1">
                                                    ИТ-интеграция
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="uni-text-content">
                                <h4>Сведение о продукте</h4>
                                <p>Контрольно-измерительный программный комплекс предназначен для автоматизированного
                                    аудита виртуальных вычислительных ресурсов информационной инфраструктуры, размещенных
                                    на платформах публичных и частных облачных провайдеров.</p>

                                <p>На основании автоматически полученной информации об объектах аудита представляет
                                    субъекту аудита результаты реакции информационной инфраструктуры на настраиваемые
                                    продолжительные воздействия.</p>

                                <blockquote>
                                    Предусмотрена возможность управления параметрами аудита вычислительных ресурсов.
                                </blockquote>

                                <div class="blockquote">
                                    <div class="blockquote-icon">
                                        <svg width="40" height="40">
                                            <use xlink:href="images/icon-collections/other-icons.svg#star-icon"></use>
                                        </svg>
                                    </div>
                                    <div class="blockquote-content">Предусмотрена возможность управления параметрами аудита вычислительных ресурсов.</div>
                                </div>

                                <h4>АУДИТ МЕХАНИЗМОВ ОБЛАЧНОЙ ИНФРАСТРУКТУРЫ</h4>

                                <p>Выполняется оценка следующих базовых механизмов:</p>

                                <ul>
                                    <li>
                                        <b>Реагирование инфраструктуры на увеличение и уменьшение нагрузки (масштабирование)</b> <br>
                                        — Увеличение масштаба при увеличении нагрузки <br>
                                        — Уменьшение масштаба при снижении нагрузки <br>
                                        — Время отклика инфраструктуры на внешние воздействия
                                    </li>

                                    <li>
                                        <b>Высокая доступность</b> <br>
                                        — Функционирование инфраструктуры в нескольких зонах доступности <br>
                                        — Кеширование данных для повышения быстродействия
                                    </li>

                                    <li>
                                        <b>Обеспечение безопасности</b> <br>
                                        — Настройка групп безопасности <br>
                                        — Распределение вычислительных ресурсов на подсети
                                    </li>

                                    <li>
                                        <b>Разворачивание и функционирование приложений</b> <br>
                                        — Автоматический запуск приложений, в том числе при масштабировании <br>
                                        — Самодиагностика приложений
                                    </li>
                                </ul>

                                <h4>Аудируемые сервисы</h4>
                                <p>Набор параметров объекта аудита включает, но не исчерпывается следующими
                                    сервисами публичных облачных провайдеров:</p>

                                <ul>
                                    <li>
                                        <b>Вычислительные ресурсы виртуальных машин</b> <br>
                                        — Тип инстанса <br>
                                        — Период восстановления
                                    </li>
                                    <li>
                                        <b>Распределение входящего трафика</b> <br>
                                        — Уровень балансировки нагрузки <br>
                                        — Целевой порт <br>
                                        — Прослушиваемый порт
                                    </li>
                                    <li>
                                        <b>Масштабирование вычислительных ресурсов</b> <br>
                                        — Группы инстансов <br>
                                        — Расчетная нагрузка
                                    </li>
                                    <li>
                                        <b>Виртуальное частное облако</b> <br>
                                        — Пул адресов виртуальной сети <br>
                                        — Набор подсетей <br>
                                        — Группы безопасности
                                    </li>
                                    <li>
                                        <b>Кеширующее хранилище</b> <br>
                                        — Адрес хранилища <br>
                                        — Размер кластера
                                    </li>
                                    <li>
                                        <b>NoSQL базы данных</b> <br>
                                        — Наименование таблицы <br>
                                        — Ключевое поле
                                    </li>
                                </ul>

                                <ol>
                                    <li>Ordered list item 1</li>
                                    <li>Ordered list item 2</li>
                                    <li>
                                        <b>Ordered list item 3</b>
                                        <ul>
                                            <li>inner ordered list 1</li>
                                            <li>inner ordered list 2</li>
                                        </ul>
                                    </li>
                                    <li>Ordered list item 4</li>
                                </ol>

                                <p><a href="#"> Simple link in the text</a></p>

                                <h1>Heading <b>H1</b></h1>
                                <h2>Heading <b>H2</b></h2>
                                <h3>Heading <b>H3</b></h3>
                                <h4>Heading <b>H4</b></h4>
                                <h5>Heading <b>H5</b></h5>
                                <h6>Heading <b>H6</b></h6>
                            </div>
                        </div>

                        <div class="offer-box page-content-offer-box sticky-box" data-margin-top="16" data-sticky-for="992" data-sticky-class="is-sticky">
                            <p class="offer-title text-heading-4">Есть вопросы? <br>
                                Напишите нам, мы всё подробно&nbsp;расскажем</p>
                            <div class="btn-wrap">
                                <a href="#" class="btn-user btn-bright btn btn-get-modal" data-modal-id="#modal-consult">
                                    <span class="btn-text">Получить консультацию</span>
                                </a>
                            </div>
                        </div>



                    </div>
                </div>
            </section>


        </div>

        <?php include "parts/footer-layout.php"; ?>
    </div>


    <?php include "parts/footer.php";?>
</body>
</html>
