<?php header("Content-Type: text/html; charset=utf-8");?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <?php include "parts/header.php";?>
</head>
<body>
    <div class="main-wrap">
        <div class="main-wrap__content">

            <div class="block-1-wrap uni-margin-small-bottom">
                <div class="bg-block-1-wrap">
                    <img src="images/bg-main.jpg" alt="nsalab" srcset="images/bg-main@2x.jpg 2x" width="1440" height="760" class="bg-block1-img">
                    <video loop="" muted="" autoplay="" poster="images/bg-main.jpg" class="bg-block1-video">
                        <source src="video/bg-video.mp4" type="video/mp4">
<!--                        <source src="video/plane.webm" type="video/webm">-->
                    </video>
                </div>

                <?php include "parts/header-layout.php"; ?>

                <section class="index-block-1-block">
                    <div class="container">
                        <div class="index-block-1">
                            <h1 class="text-heading-1">
                                Внедряем <br> IT-технологии&nbsp;—
                                <b class="with-text-carriage text-carriage-blink">увеличиваем показатели вашего бизнеса</b>
                            </h1>

                            <div class="btn-row-wrap">
                                <div class="btn-wrap btn-row">
                                    <a href="javascript:void(0)" class="btn-user btn-bright btn-row__item btn-get-modal" data-modal-id="#modal-consult">
                                        <span class="btn-text">Получить консультацию</span>
                                    </a>
                                    <a href="#" class="btn-user btn-dark-transparent btn-row__item">
                                        <span class="btn-text">Посмотреть проекты</span>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>

            <section class="directions-block uni-margin-small-top uni-margin-bottom">
                <div class="container">
                    <div class="directions">
                        <p class="title text-heading-2">Основные направления</p>
                        <p class="subtitle text-1">Мы помогаем нашим партнерам разрабатывать и внедрять инфраструктурные
                            решения для организации эффективного и&nbsp;современного образовательного процесса.</p>

                        <div class="projects-wrap _box-transform-to-row-mobile">
                            <div class="projects-box grid-box  ">
                                <div class="projects-box__item grid-item">
                                    <div class="projects-unit fill-color fill-color-orange projects-unit__directions">
                                        <div class="projects-unit__cover ">
                                            <img src="images/direction-1.svg" alt="Инфраструктура" width="320" height="196">
                                        </div>
                                        <div class="projects-unit__content">
                                            <p class="projects-unit-title text-heading-3">Инфраструктура</p>
                                            <p class="projects-desc text-1">Проведем анализ рынка современных ИТ-решений, а&nbsp;также окажем
                                                техническую экспертизу в части развития и&nbsp;модернизации вашей
                                                ИТ-инфраструктуры на этапах планирования и реализации.</p>
                                        </div>
                                        <div class="projects-unit__footer">
                                            <a href="#" class="btn-link">Посмотреть проекты</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="projects-box__item grid-item ">
                                    <div class="projects-unit fill-color fill-color-green">
                                        <div class="projects-unit__cover">
                                            <img src="images/direction-2.svg" alt="Образование" width="320" height="196">
                                        </div>
                                        <div class="projects-unit__content">
                                            <p class="projects-unit-title text-heading-3">Образование</p>
                                            <p class="projects-desc text-1">Адаптируем техническую документацию и учебные курсы производителей
                                                оборудования и&nbsp;ПО под образовательную программу колледжа,
                                                университета или учебного центра.</p>
                                        </div>
                                        <div class="projects-unit__footer">
                                            <a href="#" class="btn-link">Посмотреть проекты</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="projects-box__item grid-item ">
                                    <div class="projects-unit fill-color fill-color-blue">
                                        <div class="projects-unit__cover ">
                                            <img src="images/direction-3.svg" alt="WorldSkills" width="320" height="196">
                                        </div>
                                        <div class="projects-unit__content">
                                            <p class="projects-unit-title text-heading-3">WorldSkills</p>
                                            <p class="projects-desc text-1">Поможем вашим кандидатам показать достойные результаты на чемпионатах
                                                профессионального мастерства по ИТ-компетенциям на всех уровнях&nbsp;—
                                                от регионального до&nbsp;международного.</p>
                                        </div>
                                        <div class="projects-unit__footer">
                                            <a href="#" class="btn-link">Посмотреть проекты</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="live-digits-block uni-margin-y">
                <div class="container">
                    <div class="live-digits live-digits-wrap">
                        <div class="live-digits-slider">
                            <div class="live-digits-unit">
                                <div class="digit-title">246</div>
                                <p class="text-heading-4 desc">Компаний модернизировано</p>
                            </div>
                            <div class="live-digits-unit">
                                <div class="digit-title">10</div>
                                <p class="text-heading-4 desc">Образовательных учреждений <br> выведено на новый уровень</p>
                            </div>
                            <div class="live-digits-unit">
                                <div class="digit-title">82</div>
                                <p class="text-heading-4 desc">Победы на&nbsp;международных соревнованиях</p>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="last-projects-block uni-margin-y">
                <div class="container">
                    <div class="last-projects">
                        <div class="title-with-payload _align-items-baseline _children-mb-4px">
                            <div class="title text-heading-2">Последние проекты</div>
                            <div class="payload"><a href="#" class="text-link">Все проекты</a></div>
                        </div>

                        <div class="projects-wrap">
                            <div class="projects-box grid-box ">
                                <div class="projects-box__item grid-item">
                                    <div class="projects-unit fill-dark">
                                        <div class="projects-unit__cover projects-unit__cover_fill">
                                            <a href="#" class="projects-unit-cover-link"></a>
                                            <img src="images/project-preview-1.jpg"
                                                 alt="Тестирование облачных инфраструктур"
                                                 srcset="images/project-preview-1@2x.jpg 2x" width="368" height="240">
                                        </div>
                                        <div class="projects-unit__content">
                                            <p class="projects-unit-title text-accent-1">Тестирование облачных
                                                инфраструктур</p>
                                            <p class="projects-desc text-2">В 2020 году наша команда приняла решение
                                                о полном переходе на&nbsp;инфраструктуру публичных облачных провайдеров
                                                Amazon Web Services и&nbsp;Microsoft Azure.</p>
                                        </div>
                                        <div class="projects-unit__footer">
                                            <a href="#" class="btn-link">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="projects-box__item grid-item ">
                                    <div class="projects-unit fill-dark">
                                        <div class="projects-unit__cover projects-unit__cover_fill">
                                            <a href="#" class="projects-unit-cover-link"></a>
                                            <img src="images/project-preview-2.jpg"
                                                 alt="Подготовка национальной сборной"
                                                 srcset="images/project-preview-2@2x.jpg 2x" width="368" height="240">
                                        </div>
                                        <div class="projects-unit__content">
                                            <p class="projects-unit-title text-accent-1">Подготовка национальной сборной</p>
                                            <p class="projects-desc text-2">С 2017 года наша команда сопровождает
                                                подготовку участников национальной сборной WorldSkills Russia
                                                по&nbsp;ИТ-компетенциям.</p>
                                        </div>
                                        <div class="projects-unit__footer">
                                            <a href="#" class="btn-link">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="projects-box__item grid-item ">
                                    <div class="projects-unit fill-dark">
                                        <div class="projects-unit__cover projects-unit__cover_fill">
                                            <a href="#" class="projects-unit-cover-link"></a>
                                            <img src="images/project-preview-3.jpg"
                                                 alt="Управление сетевой инфраструктурой"
                                                 srcset="images/project-preview-3@2x.jpg 2x" width="368" height="240">
                                        </div>
                                        <div class="projects-unit__content">
                                            <p class="projects-unit-title text-accent-1">Автоматическое управление сетевой инфраструктурой</p>
                                            <p class="projects-desc text-2">Организовали структурное подразделение с целью создания необходимых
                                                условий для подготовки молодых профессионалов к чемпионатам по стандартам WorldSkills.</p>
                                        </div>
                                        <div class="projects-unit__footer">
                                            <a href="#" class="btn-link">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="technology-timeline-block uni-margin-y">
                <div class="container">
                    <div class="technology-timeline">
                        <div class="title-with-payload _children-mb-12px title-with-quote">
                            <div class="title text-heading-2">Технологический <br>таймлайн</div>
                            <div class="payload">
                                <div class="quote-unit">
                                    <div class="quote-unit__content text-2">
                                        <p>Для нас не существует ограничений в&nbsp;использовании новых технологий
                                            помогающих оптимизировать <span class="text-nowrap">критические бизнес-процессы</span></p>
                                        <p class="author-of-quote">Иванов Иван&nbsp;— Руководитель Nsalab</p>
                                    </div>
                                    <div class="quote-unit__photo">
                                        <img src="images/head-of-Nsalab.png" alt="Руководитель  Nsalab" srcset="images/head-of-Nsalab@2x.png 2x">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include "parts/slider.php"?>

                    </div>
                </div>
            </section>

            <section class="partners-block uni-margin-top">
                <div class="container">
                    <div class="partners">
                        <div class="partners-wrap">
                            <div class="partners-box grid-box">
                                <div class="partners-item grid-item">
                                    <a href="#" class="partners-unit"><img src="images/partners/cisco.svg" alt="cisco logo" width="149" height="79"></a>
                                </div>
                                <div class="partners-item grid-item">
                                    <a href="#" class="partners-unit"><img src="images/partners/amazon.svg" alt="amazon logo" width="177" height="67"></a>
                                </div>
                                <div class="partners-item grid-item">
                                    <a href="#" class="partners-unit"><img src="images/partners/azure.svg" alt="azure logo" width="217" height="63"></a>
                                </div>
                                <div class="partners-item grid-item">
                                    <a href="#" class="partners-unit"><img src="images/partners/vmware.svg" alt="vmware logo" width="191" height="31"></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

        </div>

        <?php include "parts/footer-layout.php"; ?>
    </div>


    <?php include "parts/footer.php";?>
</body>
</html>
