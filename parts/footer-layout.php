<footer class="footer-block main-wrap__edge">
    <div class="container">
        <div class="footer footer-box text-2">
            <div class="footer-author">
                <p>
                    Все права защищены  <br>
                    ©&nbsp;nsalab.org 2021
                </p>
            </div>
            <div class="footer-content">
                <p class="footer-contacts-block">
                    По вопросам сотрудничества обращайтесь по адресу электронной почты <br>
                    <a href="mailto:info@nsalab.org">info@nsalab.org</a>
                </p>
            </div>
        </div>
    </div>
</footer>
