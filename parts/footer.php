<?php include "parts/modals.php" ?>

<script src="plugins/jquery/jquery-3.2.1.min.js"></script>
<script src="plugins/jquery/jQuery.easing.1.3.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>

<script src="plugins/slick/slick.js"></script>
<script src="plugins/sticky/sticky.min.js"></script>
<script src="plugins/sly/sly.min.js"></script>
<script src="plugins/light-modal/simple-modal.js"></script>
<script src="plugins/jquery-mask/jquery-mask.js"></script>
<!--
<script src="plugins/lightgallery/js/lightgallery-all.js"></script>
light gallery contains so much code. Let's use modules better.
-->
<script src="plugins/lightgallery/js/lightgallery.min.js"></script>
<script src="plugins/lightgallery/modules/lg-video.min.js"></script>

<script src="js/app.js"></script>
<script src="js/slider-gallery.js"></script>



