<header class="header-block">
    <div class="container">
        <div class="header">
            <div class="head-item head-logo"><a href="/" class="logo"><img src="images/nsalab-logo.svg" alt="logo" width="174" height="32"></a></div>
            <div class="head-item  head-nav nav-wrap">
                <div class="btn-menu btn-menu-close" data-action="menu-close">
                    <svg width="329" height="329">
                        <use xlink:href="images/icon-collections/other-icons.svg#close-icon"></use>
                    </svg>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a href="#" class="active">Наши проекты</a></li>
                    <li class="nav-item"><a href="#">Готовые решения</a></li>
                    <li class="nav-item"><a href="#">О нас</a></li>
                    <li class="nav-item"><a href="#">Контакты</a></li>
                </ul>
            </div>
            <div class="btn-menu btn-menu-open" data-action="menu-open">
                <svg width="24" height="24">
                    <use xlink:href="images/icon-collections/other-icons.svg#menu-icon"></use>
                </svg>
            </div>
        </div>
    </div>
</header>
