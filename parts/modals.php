<div class="modal modal-block" id="modal-consult">

    <div class="btn-modal-close" data-closable="true">
        <svg width="24" height="24">
            <use xlink:href="images/icon-collections/other-icons.svg#close-sm-icon"></use>
        </svg>
    </div>
    <p class="modal-title text-heading-2">Расскажем во всех подробностях</p>

    <form class="send-form modal-form">
        <div class="input-item">
            <input type="text" class="uni-input validated-control" name="name" placeholder="Имя">
        </div>
        <div class="input-item">
            <input type="tel" class="uni-input validated-control" name="phone" placeholder="Номер телефона">
        </div>


        <div class="policy-check-wrap">
            <label class="uni-check-block">
                <input name="policy" type="checkbox" class="validated-control">
                <span class="check-block-icon"></span>
                <span class="check-block-text">Даю согласие на обработку персональных данных</span>
            </label>
        </div>

        <input type="hidden" name="info" value="Получить консультацию">

        <div class="btn-submit-wrap">
            <button type="submit" class="btn-user btn-bright btn-submit"><span class="btn-text">Получить консультацию</span></button>
        </div>

    </form>
</div>

<div class="modal modal-success-block" id="modal-success">
    <div class="modal-success-icon"><img src="images/fire-icon.png" alt="Огонь!" srcset="images/fire-icon@2x.png" width="80" height="80"></div>
    <p class="modal-success-title text-heading-2">
        Заявка отправлена. <br>
        В ближайшее время мы вам позвоним.
    </p>
</div>