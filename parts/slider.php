
<div class="tech-timeline tech-timeline-projects-box">
    <div class="timeline-slider">
        <div class="frame timeline-slider__slides">
            <div class="timeline-item text-heading-1">2012</div>
            <div class="timeline-item text-heading-1">2013</div>
            <div class="timeline-item text-heading-1">2014</div>
            <div class="timeline-item text-heading-1">2015</div>
            <div class="timeline-item text-heading-1">2016</div>
            <div class="timeline-item text-heading-1">2017</div>
            <div class="timeline-item text-heading-1">2018</div>
            <div class="timeline-item text-heading-1">2019</div>
            <div class="timeline-item text-heading-1">2020</div>
            <div class="timeline-item text-heading-1">2021</div>
        </div>
    </div>

    <div class="projects-slider-outer slider-outer">
        <div class="btn-slider-prev btn-slider"><img src="images/arrow-slider-icon.svg" alt="" width="40" height="40"></div>
        <div class="btn-slider-next btn-slider"><img src="images/arrow-slider-icon.svg" alt="" width="40" height="40"></div>

        <div class="projects-slider">
            <div class="frame projects-slider__slides">

                <div class="project-box-unit">
                    <div class="project-box-media project-one-slider">

                        <div class="project-box-media-main project-one-slider-main lg-box"
                             data-html="#uniq-id-video-1"
                        >
                                             <span class="play-icon">
                                                    <svg width="52" height="52">
                                                        <use xlink:href="images/icon-collections/other-icons.svg#play-icon"></use>
                                                    </svg>
                                                </span>
                            <img src="images/bg-main.jpg" alt="">
                        </div>

                        <div class="project-box-media-preview-box project-one-slider-preview">
                            <div class="item">
                                <div class="project-box-media-preview-unit project-one-slider-preview-item active"
                                     data-img-preview="images/bg-main.jpg"
                                     data-html-video="#uniq-id-video-1"
                                >
                                    <div class="play-icon">
                                        <svg width="52" height="52">
                                            <use xlink:href="images/icon-collections/other-icons.svg#play-icon"></use>
                                        </svg>
                                    </div>
                                    <img src="images/bg-main.jpg" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="project-box-media-preview-unit project-one-slider-preview-item "
                                     data-img-preview="images/project-preview-4.jpg"
                                     data-video="https://www.youtube.com/watch?v=yg4Mq5EAEzw"
                                >
                                    <div class="play-icon">
                                        <svg width="52" height="52">
                                            <use xlink:href="images/icon-collections/other-icons.svg#play-icon"></use>
                                        </svg>
                                    </div>
                                    <img src="images/project-preview-4.jpg" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="project-box-media-preview-unit project-one-slider-preview-item"
                                     data-img-preview="images/project-preview-wide-1.jpg"
                                     data-img-full="images/project-preview-wide-1@2x.jpg"
                                >
                                    <img src="images/project-preview-wide-1.jpg" alt="">
                                </div>
                            </div>
                        </div>

                        <div class="project-box-media-disabled-block">
                            <div id="uniq-id-video-1">
                                <video class="lg-video-object lg-html5 video-js vjs-default-skin" controls preload="none">
                                    <source src="video/bg-video.mp4" type="video/mp4">
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                        </div>
                    </div>

                    <div class="project-box-content text-2">
                        <p class="title text-heading-3">Вложенная виртуализация</p>
                        <p>Наша команда провела первую опытную эксплуатацию данного решения в рамках II
                            регионального чемпионата WorldSkills по компетенции "Сетевое и системное
                            администрирование". Данное решение до сих пор является де-факто стандартом
                            организации инфраструктуры для российских и международных чемпионатов
                            по данной компетенции.</p>
                    </div>
                </div>

                <!---->

                <div class="project-box-unit">
                    <div class="project-box-media project-one-slider">

                        <div class="project-box-media-main project-one-slider-main lg-box"
                             data-html="#uniq-id-video-2"
                        >
                                             <span class="play-icon">
                                                    <svg width="52" height="52">
                                                        <use xlink:href="images/icon-collections/other-icons.svg#play-icon"></use>
                                                    </svg>
                                                </span>
                            <img src="images/bg-main.jpg" alt="">
                        </div>

                        <div class="project-box-media-preview-box project-one-slider-preview">
                            <div class="item">
                                <div class="project-box-media-preview-unit project-one-slider-preview-item active"
                                     data-img-preview="images/bg-main.jpg"
                                     data-html-video="#uniq-id-video-2"
                                >
                                    <div class="play-icon">
                                        <svg width="52" height="52">
                                            <use xlink:href="images/icon-collections/other-icons.svg#play-icon"></use>
                                        </svg>
                                    </div>
                                    <img src="images/bg-main.jpg" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="project-box-media-preview-unit project-one-slider-preview-item "
                                     data-img-preview="images/project-preview-4.jpg"
                                     data-video="https://www.youtube.com/watch?v=yg4Mq5EAEzw"
                                >
                                    <div class="play-icon">
                                        <svg width="52" height="52">
                                            <use xlink:href="images/icon-collections/other-icons.svg#play-icon"></use>
                                        </svg>
                                    </div>
                                    <img src="images/project-preview-4.jpg" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="project-box-media-preview-unit project-one-slider-preview-item"
                                     data-img-preview="images/project-preview-wide-1.jpg"
                                     data-img-full="images/project-preview-wide-1@2x.jpg"
                                >
                                    <img src="images/project-preview-wide-1.jpg" alt="">
                                </div>
                            </div>
                        </div>

                        <div class="project-box-media-disabled-block">
                            <div id="uniq-id-video-2">
                                <video class="lg-video-object lg-html5 video-js vjs-default-skin" controls preload="none">
                                    <source src="video/bg-video.mp4" type="video/mp4">
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                        </div>
                    </div>

                    <div class="project-box-content text-2">
                        <p class="title text-heading-3">Вложенная виртуализация</p>
                        <p>Наша команда провела первую опытную эксплуатацию данного решения в рамках II
                            регионального чемпионата WorldSkills по компетенции "Сетевое и системное
                            администрирование". Данное решение до сих пор является де-факто стандартом
                            организации инфраструктуры для российских и международных чемпионатов
                            по данной компетенции.</p>
                    </div>
                </div>



            </div>
        </div>
    </div>

</div>













</body>
</html>
