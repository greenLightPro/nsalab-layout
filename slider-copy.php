<?php header("Content-Type: text/html; charset=utf-8");?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <?php include "parts/header.php";?>
</head>
<body>
<div class="main-wrap">
    <div class="main-wrap__content">

        <section class="tech-timeline-block">
            <div class="container">
                <div class="tech-timeline tech-timeline-projects-box">
                    <div class="timeline-slider">
                        <div class="frame timeline-slider__slides">
                            <div class="timeline-item text-heading-1">2012</div>
                            <div class="timeline-item text-heading-1">2013</div>
                            <div class="timeline-item text-heading-1">2014</div>
                            <div class="timeline-item text-heading-1">2015</div>
                            <div class="timeline-item text-heading-1">2016</div>
                            <div class="timeline-item text-heading-1">2017</div>
                            <div class="timeline-item text-heading-1">2018</div>
                            <div class="timeline-item text-heading-1">2019</div>
                            <div class="timeline-item text-heading-1">2020</div>
                            <div class="timeline-item text-heading-1">2021</div>
                        </div>
                    </div>

                    <div class="projects-slider-outer slider-outer">
                        <div class="btn-slider-prev btn-slider"><img src="images/arrow-slider-icon.svg" alt="" width="40" height="40"></div>
                        <div class="btn-slider-next btn-slider"><img src="images/arrow-slider-icon.svg" alt="" width="40" height="40"></div>

                        <div class="projects-slider">
                            <div class="frame projects-slider__slides">

                                <div class="project-box-unit">
                                    <div class="project-box-media project-one-slider">

                                        <div class="project-box-media-main project-one-slider-main lg-box"
                                             data-html="#uniq-id-video-1"
                                        >
                                             <span class="play-icon">
                                                    <svg width="52" height="52">
                                                        <use xlink:href="images/icon-collections/other-icons.svg#play-icon"></use>
                                                    </svg>
                                                </span>
                                            <img src="images/bg-main.jpg" alt="">
                                        </div>

                                        <div class="project-box-media-preview-box project-one-slider-preview">
                                            <div class="item">
                                                <div class="project-box-media-preview-unit project-one-slider-preview-item active"
                                                     data-img-preview="images/bg-main.jpg"
                                                     data-html-video="#uniq-id-video-1"
                                                >
                                                    <div class="play-icon">
                                                        <svg width="52" height="52">
                                                            <use xlink:href="images/icon-collections/other-icons.svg#play-icon"></use>
                                                        </svg>
                                                    </div>
                                                    <img src="images/bg-main.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="project-box-media-preview-unit project-one-slider-preview-item "
                                                     data-img-preview="images/project-preview-4.jpg"
                                                     data-video="https://www.youtube.com/watch?v=yg4Mq5EAEzw"
                                                >
                                                    <div class="play-icon">
                                                        <svg width="52" height="52">
                                                            <use xlink:href="images/icon-collections/other-icons.svg#play-icon"></use>
                                                        </svg>
                                                    </div>
                                                    <img src="images/project-preview-4.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="project-box-media-preview-unit project-one-slider-preview-item"
                                                     data-img-preview="images/project-preview-wide-1.jpg"
                                                     data-img-full="images/project-preview-wide-1@2x.jpg"
                                                >
                                                    <img src="images/project-preview-wide-1.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-box-media-disabled-block">
                                            <div id="uniq-id-video-1">
                                                <video class="lg-video-object lg-html5 video-js vjs-default-skin" controls preload="none">
                                                    <source src="video/bg-video.mp4" type="video/mp4">
                                                    Your browser does not support HTML5 video.
                                                </video>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="project-box-content text-2">
                                        <p class="title text-heading-3">Вложенная виртуализация</p>
                                        <p>Наша команда провела первую опытную эксплуатацию данного решения в рамках II
                                            регионального чемпионата WorldSkills по компетенции "Сетевое и системное
                                            администрирование". Данное решение до сих пор является де-факто стандартом
                                            организации инфраструктуры для российских и международных чемпионатов
                                            по данной компетенции.</p>
                                    </div>
                                </div>

                                <!---->

                                <div class="project-box-unit">
                                    <div class="project-box-media project-one-slider">

                                        <div class="project-box-media-main project-one-slider-main lg-box"
                                             data-html="#uniq-id-video-2"
                                        >
                                             <span class="play-icon">
                                                    <svg width="52" height="52">
                                                        <use xlink:href="images/icon-collections/other-icons.svg#play-icon"></use>
                                                    </svg>
                                                </span>
                                            <img src="images/bg-main.jpg" alt="">
                                        </div>

                                        <div class="project-box-media-preview-box project-one-slider-preview">
                                            <div class="item">
                                                <div class="project-box-media-preview-unit project-one-slider-preview-item active"
                                                     data-img-preview="images/bg-main.jpg"
                                                     data-html-video="#uniq-id-video-2"
                                                >
                                                    <div class="play-icon">
                                                        <svg width="52" height="52">
                                                            <use xlink:href="images/icon-collections/other-icons.svg#play-icon"></use>
                                                        </svg>
                                                    </div>
                                                    <img src="images/bg-main.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="project-box-media-preview-unit project-one-slider-preview-item "
                                                     data-img-preview="images/project-preview-4.jpg"
                                                     data-video="https://www.youtube.com/watch?v=yg4Mq5EAEzw"
                                                >
                                                    <div class="play-icon">
                                                        <svg width="52" height="52">
                                                            <use xlink:href="images/icon-collections/other-icons.svg#play-icon"></use>
                                                        </svg>
                                                    </div>
                                                    <img src="images/project-preview-4.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="project-box-media-preview-unit project-one-slider-preview-item"
                                                     data-img-preview="images/project-preview-wide-1.jpg"
                                                     data-img-full="images/project-preview-wide-1@2x.jpg"
                                                >
                                                    <img src="images/project-preview-wide-1.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-box-media-disabled-block">
                                            <div id="uniq-id-video-2">
                                                <video class="lg-video-object lg-html5 video-js vjs-default-skin" controls preload="none">
                                                    <source src="video/bg-video.mp4" type="video/mp4">
                                                    Your browser does not support HTML5 video.
                                                </video>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="project-box-content text-2">
                                        <p class="title text-heading-3">Вложенная виртуализация</p>
                                        <p>Наша команда провела первую опытную эксплуатацию данного решения в рамках II
                                            регионального чемпионата WorldSkills по компетенции "Сетевое и системное
                                            администрирование". Данное решение до сих пор является де-факто стандартом
                                            организации инфраструктуры для российских и международных чемпионатов
                                            по данной компетенции.</p>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>


    </div>

    <?php include "parts/footer-layout.php"; ?>
</div>





<?php include "parts/footer.php";?>

<script>
    const timeLineSliderOptions = {

        horizontal: 1 ,
        itemNav: 'centered',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt:  0,
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1,
    };
    const timeLineSlider = new Sly('.timeline-slider', timeLineSliderOptions);
    timeLineSlider.init();

    const projectsSliderOptions = {
        horizontal: 1 ,
        itemNav: 'basic',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt:  0,
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1,
    };

    function projectsSliderInit(selector, options) {
        const projectsSlider = document.querySelector(selector);
        const projectsSliderOuter = projectsSlider.closest('.slider-outer');
        const btnSliderPrev = projectsSliderOuter.querySelector('.btn-slider-prev');
        const btnSliderNext = projectsSliderOuter.querySelector('.btn-slider-next');

        const extOptions = Object.assign(options, {
            prev: btnSliderPrev,
            next: btnSliderNext,
        });

        const slider = new Sly(selector, extOptions);
        slider.init();
    }

    projectsSliderInit('.projects-slider', projectsSliderOptions);

    const projectOneSliderList = document.querySelectorAll('.project-one-slider')


    projectOneSliderList.forEach(slider => {
        const mainPreview = slider.querySelector('.project-one-slider-main');

        const $mainPreviewGallery = $(mainPreview);
        $mainPreviewGallery.lightGallery({
            selector: 'this',
        });

        slider.addEventListener('click', function (evt) {
            const evtTarget = evt.target;
            const previewItem = evtTarget.closest('.project-one-slider-preview-item');
            if (previewItem === null || previewItem.classList.contains('active') ) return false;
            const dataImgPreview = previewItem.dataset.imgPreview;
            const dataImgFull = previewItem.dataset.imgFull;
            const dataVideo = previewItem.dataset.video;
            const dataHtmlVideo = previewItem.dataset.htmlVideo;

            const playIconHtml = `
                         <span class="play-icon">
                            <svg width="52" height="52">
                                <use xlink:href="images/icon-collections/other-icons.svg#play-icon"></use>
                            </svg>
                        </span>
            `;

            let mainSlideHtml = '';

            if (dataImgPreview && dataImgPreview !== '') {
                mainSlideHtml += `<img src="${dataImgPreview}" alt="">`;
            }

            if (dataImgFull && dataImgFull !== '') {
                $mainPreviewGallery.removeAttr('data-html');
                $mainPreviewGallery.attr('data-src', `${dataImgFull}` );
            }

            if (dataVideo && dataVideo !== '') {
                $mainPreviewGallery.removeAttr('data-html');
                $mainPreviewGallery.attr('data-src', `${dataVideo}` );
                mainSlideHtml += playIconHtml;
            }

            if (dataHtmlVideo && dataHtmlVideo !== '') {
                $mainPreviewGallery.removeAttr('data-src');
                $mainPreviewGallery.attr('data-html', `${dataHtmlVideo}` );
                mainSlideHtml += playIconHtml;
            }

            if (mainSlideHtml === '') return false;

            $mainPreviewGallery.html(mainSlideHtml);

            slider.querySelectorAll('.project-one-slider-preview-item').forEach(el => {
                el.classList.remove('active');
            });
            previewItem.classList.add('active');
        });
    });

</script>












</body>
</html>
