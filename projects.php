<?php header("Content-Type: text/html; charset=utf-8");?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <?php include "parts/header.php";?>
</head>
<body>
<div class="main-wrap">
    <div class="main-wrap__content">
        <?php include "parts/header-layout.php"; ?>
        <section class="projects-block">
            <div class="container">
                <div class="projects">
                    <p class="text-heading-2 title">Выполненные проекты</p>
                    <div class="tags-wrap">
                        <ul class="tags-box">
                            <li class="tag-item"><a href="#" class="tag-unit tag-active">Все</a></li>
                            <li class="tag-item"><a href="#" class="tag-unit">Инфрафструктура</a></li>
                            <li class="tag-item"><a href="#" class="tag-unit">Worldskills</a></li>
                            <li class="tag-item"><a href="#" class="tag-unit">Образование</a></li>
                        </ul>
                    </div>

                    <div class="projects-wrap">
                        <div class="projects-box grid-box ">
                            <div class="projects-box__item grid-item">
                                <div class="projects-unit fill-dark">
                                    <div class="projects-unit__cover projects-unit__cover_fill">
                                        <a href="#" class="projects-unit-cover-link"></a>
                                        <img src="images/project-preview-1.jpg"
                                             alt="Тестирование облачных инфраструктур"
                                             srcset="images/project-preview-1@2x.jpg 2x" width="368" height="240">
                                    </div>
                                    <div class="projects-unit__content">
                                        <p class="projects-unit-title text-accent-1">Тестирование облачных
                                            инфраструктур</p>
                                        <p class="projects-desc text-2">В 2020 году наша команда приняла решение
                                            о полном переходе на&nbsp;инфраструктуру публичных облачных провайдеров
                                            Amazon Web Services и&nbsp;Microsoft Azure.</p>
                                    </div>
                                    <div class="projects-unit__footer">
                                        <a href="#" class="btn-link">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                            <div class="projects-box__item grid-item ">
                                <div class="projects-unit fill-dark">
                                    <div class="projects-unit__cover projects-unit__cover_fill">
                                        <a href="#" class="projects-unit-cover-link"></a>
                                        <img src="images/project-preview-2.jpg"
                                             alt="Подготовка национальной сборной"
                                             srcset="images/project-preview-2@2x.jpg 2x" width="368" height="240">
                                    </div>
                                    <div class="projects-unit__content">
                                        <p class="projects-unit-title text-accent-1">Подготовка национальной сборной</p>
                                        <p class="projects-desc text-2">С 2017 года наша команда сопровождает
                                            подготовку участников национальной сборной WorldSkills Russia
                                            по&nbsp;ИТ-компетенциям.</p>
                                    </div>
                                    <div class="projects-unit__footer">
                                        <a href="#" class="btn-link">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                            <div class="projects-box__item grid-item ">
                                <div class="projects-unit fill-dark">
                                    <div class="projects-unit__cover projects-unit__cover_fill">
                                        <a href="#" class="projects-unit-cover-link"></a>
                                        <img src="images/project-preview-3.jpg"
                                             alt="Управление сетевой инфраструктурой"
                                             srcset="images/project-preview-3@2x.jpg 2x" width="368" height="240">
                                    </div>
                                    <div class="projects-unit__content">
                                        <p class="projects-unit-title text-accent-1">Автоматическое управление сетевой инфраструктурой</p>
                                        <p class="projects-desc text-2">Организовали структурное подразделение с целью создания необходимых
                                            условий для подготовки молодых профессионалов к чемпионатам по стандартам WorldSkills.</p>
                                    </div>
                                    <div class="projects-unit__footer">
                                        <a href="#" class="btn-link">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn-load-wrap">
                        <div class="btn-load btn-user btn-dark">
                            <div class="btn-text">Загрузить еще</div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </div>

    <?php include "parts/footer-layout.php"; ?>
</div>


<?php include "parts/footer.php";?>
</body>
</html>

