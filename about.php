<?php header("Content-Type: text/html; charset=utf-8");?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <?php include "parts/header.php";?>
</head>
<body>
    <div class="main-wrap">
        <div class="main-img-layout main-img-layout-planet "><img src="images/digital-planet.jpg" alt="Планета" srcset="images/digital-planet@2x.jpg 2x" width="1237" height="1122"></div>
        <div class="main-wrap__content">
            <?php include "parts/header-layout.php"; ?>
            <div class="company-main-block">
                <div class="container">
                    <div class="company-main">
                        <h1 class="text-heading-1 h1">
                            Лаборатория администрирования сетевых систем&nbsp;— <b>NSALAB</b>
                        </h1>
                        <p class="text-1 short-container desc">
                            Наша цель — разработка и внедрение инфраструктурных решений
                            для организации эффективного образовательного процесса
                            и&nbsp;проведения соревнований по компетенциям в сфере информационных технологий
                        </p>
                    </div>
                </div>
            </div>

            <section class="team-block uni-margin-top">
                <div class="container">
                    <div class="team">
                        <p class="title text-heading-2">Команда</p>
                        <div class="team-wrap">
                            <div class="team-box grid-box">
                                
                                <div class="team-item grid-item">
                                    <div class="team-unit">
                                        <div class="team-unit__photo"><img src="images/staff-1.png" alt="Александр Горбачев" srcset="images/staff-1@2x.png 2x" width="172" height="172"></div>
                                        <div class="team-unit__content">
                                            <p class="name text-heading-4">Александр <br> Горбачев</p>
                                            <p class="desc text-1">NSALAB <br> Consulting engineer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="team-item grid-item">
                                    <div class="team-unit">
                                        <div class="team-unit__photo"><img src="images/staff-2.png" alt="Руслан Гассеев" srcset="images/staff-2@2x.png 2x" width="172" height="172"></div>
                                        <div class="team-unit__content">
                                            <p class="name text-heading-4">Руслан <br> Гассеев</p>
                                            <p class="desc text-1">АО ИК «АСЭ» <br> Ведущий специалист ОИСТ</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="team-item grid-item">
                                    <div class="team-unit">
                                        <div class="team-unit__photo"><img src="images/staff-3.png" alt="Рузиль Мухаметов" srcset="images/staff-3@2x.png 2x" width="172" height="172"></div>
                                        <div class="team-unit__content">
                                            <p class="name text-heading-4">Рузиль <br> Мухаметов</p>
                                            <p class="desc text-1">Dino Systems <br> SysOps engineer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="team-item grid-item">
                                    <div class="team-unit">
                                        <div class="team-unit__photo"><img src="images/staff-4.png" alt="Владислав Тетюшкин" srcset="images/staff-4@2x.png 2x" width="172" height="172"></div>
                                        <div class="team-unit__content">
                                            <p class="name text-heading-4">Владислав <br> Тетюшкин</p>
                                            <p class="desc text-1">LANSIDE <br> Ведущий инженер</p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="geography-block uni-margin-small-top">
                <div class="container">
                    <div class="geography">
                        <p class="text-heading-2 title">География</p>
                        <p class="text-1 short-container">В период с 2018 г. по 2019 г. наша команда успешно провела национальные,
                            отборочные и международные чемпионаты по стандартам WorldSkills в таких странах как ОАЭ,
                            Германия, Великобритания, Китай, Австралия, Канада, Индия, Россия и Казахстан </p>
                    </div>
                </div>
            </section>

            <section class="achievement-block uni-margin-small-top">
                <div class="container">
                    <div class="achievement">
                        <p class="title text-heading-2">Достижения</p>
                        <div class="achievement-wrap">
                            <div class="achievement-box grid-box">

                                <div class="achievement-item grid-item">
                                    <div class="achievement-unit">
                                        <div class="achievement-icon">
                                            <img src="images/frame-medal-gold.png" alt="Золото" srcset="images/frame-medal-gold@2x.png 2x" width="145" height="138">
                                        </div>
                                        <div class="achievement-content">
                                            <div class="achievement-heading text-heading-4">Сетевое и системное администрирование</div>
                                            <div class="achievement-desc text-1">
                                                WorldSkills <br>
                                                International 2017
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="achievement-item grid-item">
                                    <div class="achievement-unit">
                                        <div class="achievement-icon">
                                            <img src="images/frame-medal-gold-bronze.png" alt="Золото и бронза" srcset="images/frame-medal-gold-bronze@2x.png 2x" width="145" height="138">
                                        </div>
                                        <div class="achievement-content">
                                            <div class="achievement-heading text-heading-4">Сетевое и системное администрирование&nbsp;/ Облачные технологии</div>
                                            <div class="achievement-desc text-1">
                                                Global Skills Challenge 2019
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="achievement-item grid-item">
                                    <div class="achievement-unit">
                                        <div class="achievement-icon">
                                            <img src="images/frame-medal-gold.png" alt="Золото" srcset="images/frame-medal-gold@2x.png 2x" width="145" height="138">
                                        </div>
                                        <div class="achievement-content">
                                            <div class="achievement-heading text-heading-4">Сетевое и системное администрирование</div>
                                            <div class="achievement-desc text-1">
                                                WorldSkills <br>
                                                International 2018
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="achievement-item grid-item">
                                    <div class="achievement-unit">
                                        <div class="achievement-icon">
                                            <img src="images/frame-medal-gold.png" alt="Золото" srcset="images/frame-medal-gold@2x.png 2x" width="145" height="138">
                                        </div>
                                        <div class="achievement-content">
                                            <div class="achievement-heading text-heading-4">Кибербезопасность</div>
                                            <div class="achievement-desc text-1">
                                                WorldSkills <br>
                                                International 2019
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>

        <?php include "parts/footer-layout.php"; ?>
    </div>


    <?php include "parts/footer.php";?>
</body>
</html>
