/**
 * slick slider [live-digits-slider]
 * @param selector
 * @returns {boolean}
 */

const initLiveDigitsSlider = function(selector) {
    const $liveDigitsSlider = $(selector);
    if (!$liveDigitsSlider.length > 0) return false;
    $liveDigitsSlider.slick({
        dots: false,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    variableWidth: true
                }
            }
        ]
    });
};

/**
 * Menu
 */

const initAdaptiveMenu = function() {
    const closeMenuHandler = function (evt) {
        const evtTarget = evt.target;
        if (
            !(!( evtTarget.closest('.btn-menu') || evtTarget.closest('.nav-wrap') ) || evtTarget.closest('.nav a'))
        ) return false;
        document.body.classList.remove('menu-active');
        document.querySelector('.nav').scrollTop = 0;
        document.removeEventListener('click', closeMenuHandler);
    };

    document.addEventListener('click', function (evt) {
        const evtTarget = evt.target;
        if (evtTarget.closest('.btn-menu')) {
            const $btnMenu = evtTarget.closest('.btn-menu');

            if ($btnMenu.dataset.action === 'menu-open') {
                document.body.classList.add('menu-active');
                document.addEventListener('click', closeMenuHandler)
                return 0;
            }
            document.querySelector('.nav').scrollTop = 0;
            document.body.classList.remove('menu-active');
        }
    });
};

/**
 * Sticky
 * @param selector
 * @returns {boolean}
 */
const initSticky = function(selector) {
    if (document.querySelectorAll(selector).length === 0) return false;
    const sticky = new Sticky(selector);
};

/**
 * Modals
 * @type {{destruct: destruct, close: close, open: open}}
 */

const addSiteBlurEffect = function() {
    document.body.classList.add('site-blur-effect');
}
const removeSiteBlurEffect = function() {
    document.body.classList.remove('site-blur-effect');
}

// let's register all modal frames.
const modalConsult = fnPlugin.modal( '#modal-consult' ,{
    animationIn: 'fadeInDown',
    animationOut: 'fadeOutUp',
    animationDuration: 300,
    classNames: ['outer-modal-consult'],
    beforeOpen: addSiteBlurEffect,
    beforeClose: removeSiteBlurEffect,
});
const modalSuccess = fnPlugin.modal( '#modal-success' ,{
    animationIn: 'fadeInDown',
    animationOut: 'fadeOutUp',
    animationDuration: 300,
    classNames: ['outer-modal-success'],
    beforeOpen: addSiteBlurEffect,
    beforeClose: removeSiteBlurEffect,
});

const btnOpenModalHandler = function(evt) {
    const evtTarget = evt.target;
    const el = evtTarget.closest('.btn-get-modal');
    if (el === null) return false;
    evt.preventDefault();

    const elSelector = el.dataset.modalId;
    // enumerate selectors of modal frames in switch-case
    switch (elSelector) {
        case '#modal-consult': {
            modalConsult.open();
        }
    }
};


$(document).ready(function() {
    initAdaptiveMenu();
    initLiveDigitsSlider('.live-digits-slider');
    initSticky('.sticky-box');

    document.addEventListener('click', btnOpenModalHandler);
});


/**
 * Работа с элементами формы
 * отправка форм
 */

/**
 * Mask for input[type="tel"]
 */
const initMaskPhoneInputs = function() {
    const inputPhones = $('input[type="tel"]');

    if(inputPhones.length) {
        inputPhones.mask('+7 000 000 00 00');
        inputPhones.on('focus', function () {
            const $this = $(this);
            if ($this.val().length === 0) {
                $this.val('+7 ');
            }
        });
        inputPhones.on('blur', function () {
            const $this = $(this);
            if ($this.val().length !== 16) {
                $this.val('');
            }
        });
    }
}

/**
 * Validation of form
 * need to add class "validated-control" to input for its validation
 * @param jqObj
 * @returns {boolean}
 */

const isValid = function (jqObj) {
    const objects = jqObj.find('.validated-control');
    let mas = [];
    objects.each(function () {
        const $this = $(this);
        if ($this.val().length === 0 || !$this.val().replace(/\s+/g, '')) {
            $this.addClass('invalid');
            mas.push("0");
        } else if (($this.attr('type') === "checkbox") && !$this.prop('checked')) {
            $this.addClass('invalid');
            mas.push("0");
        } else if ($this.attr('type')=== "tel" && $this.val().length !== 16) {
            $this.addClass('invalid');
            mas.push("0");
        }
        else{
            $this.removeClass('invalid');
        }
    });
    return (mas.length === 0);
}

/**
 * Wrap for send form
 */

const initSendForms = function () {
    const $sendForms = $('.send-form');
    if ($sendForms.length) {
        $sendForms.submit(function(e) {
            e.preventDefault();
            const $form = $(this);
            const formData =  new FormData($form[0]);

            if (isValid($form)) {
                $.ajax({
                    type: "POST",
                    url: "/php/mail.php",
                    data: formData,
                    contentType:false,
                    processData:false,
                    success: function (res) {
                        $form.trigger('reset');
                        if ($form.closest('.light-modal-outer').length > 0) {
                            $form.closest('.light-modal-outer')[0].modal.close();
                        }
                        const setTimeViewModalOpen = setTimeout(function (){
                            modalSuccess.open();
                            clearTimeout(setTimeViewModalOpen);
                        }, 300)

                        const setTimeViewModalClose = setTimeout(function () {
                            modalSuccess.close();
                            clearTimeout(setTimeViewModalClose);
                        }, 4000);
                    }
                });
            }
            return false;
        });
    }
};


$(document).ready(function () {
    initMaskPhoneInputs();

    $(document).on('focus change', 'input, textarea', function(){
        $(this).removeClass('invalid');
    });

    initSendForms();
});

