const timeLineSliderOptions = {

    horizontal: 1 ,
    itemNav: 'centered',
    smart: 1,
    activateOn: 'click',
    mouseDragging: 1,
    touchDragging: 1,
    releaseSwing: 1,
    startAt:  0,
    speed: 300,
    elasticBounds: 1,
    easing: 'easeOutExpo',
    dragHandle: 1,
    dynamicHandle: 1,
    clickBar: 1,
};
const timeLineSlider = new Sly('.timeline-slider', timeLineSliderOptions);
timeLineSlider.init();

const projectsSliderOptions = {
    horizontal: 1 ,
    itemNav: 'basic',
    smart: 1,
    activateOn: 'click',
    mouseDragging: 1,
    touchDragging: 1,
    releaseSwing: 1,
    startAt:  0,
    speed: 300,
    elasticBounds: 1,
    easing: 'easeOutExpo',
    dragHandle: 1,
    dynamicHandle: 1,
    clickBar: 1,
};

function projectsSliderInit(selector, options) {
    const projectsSlider = document.querySelector(selector);
    const projectsSliderOuter = projectsSlider.closest('.slider-outer');
    const btnSliderPrev = projectsSliderOuter.querySelector('.btn-slider-prev');
    const btnSliderNext = projectsSliderOuter.querySelector('.btn-slider-next');

    const extOptions = Object.assign(options, {
        prev: btnSliderPrev,
        next: btnSliderNext,
    });

    const slider = new Sly(selector, extOptions);
    slider.init();
}

projectsSliderInit('.projects-slider', projectsSliderOptions);

const projectOneSliderList = document.querySelectorAll('.project-one-slider')


projectOneSliderList.forEach(slider => {
    const mainPreview = slider.querySelector('.project-one-slider-main');

    const $mainPreviewGallery = $(mainPreview);
    $mainPreviewGallery.lightGallery({
        selector: 'this',
    });

    slider.addEventListener('click', function (evt) {
        const evtTarget = evt.target;
        const previewItem = evtTarget.closest('.project-one-slider-preview-item');
        if (previewItem === null || previewItem.classList.contains('active') ) return false;
        const dataImgPreview = previewItem.dataset.imgPreview;
        const dataImgFull = previewItem.dataset.imgFull;
        const dataVideo = previewItem.dataset.video;
        const dataHtmlVideo = previewItem.dataset.htmlVideo;

        const playIconHtml = `
                         <span class="play-icon">
                            <svg width="52" height="52">
                                <use xlink:href="images/icon-collections/other-icons.svg#play-icon"></use>
                            </svg>
                        </span>
            `;

        let mainSlideHtml = '';

        if (dataImgPreview && dataImgPreview !== '') {
            mainSlideHtml += `<img src="${dataImgPreview}" alt="">`;
        }

        if (dataImgFull && dataImgFull !== '') {
            $mainPreviewGallery.removeAttr('data-html');
            $mainPreviewGallery.attr('data-src', `${dataImgFull}` );
        }

        if (dataVideo && dataVideo !== '') {
            $mainPreviewGallery.removeAttr('data-html');
            $mainPreviewGallery.attr('data-src', `${dataVideo}` );
            mainSlideHtml += playIconHtml;
        }

        if (dataHtmlVideo && dataHtmlVideo !== '') {
            $mainPreviewGallery.removeAttr('data-src');
            $mainPreviewGallery.attr('data-html', `${dataHtmlVideo}` );
            mainSlideHtml += playIconHtml;
        }

        if (mainSlideHtml === '') return false;

        $mainPreviewGallery.html(mainSlideHtml);

        slider.querySelectorAll('.project-one-slider-preview-item').forEach(el => {
            el.classList.remove('active');
        });
        previewItem.classList.add('active');
    });
});
