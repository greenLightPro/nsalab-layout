<?php header("Content-Type: text/html; charset=utf-8");?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <?php include "parts/header.php";?>
</head>
<body>
    <div class="main-wrap">
        <div class="main-img-layout main-img-layout-map">
            <img src="images/map-layer.jpg" alt="Планета" srcset="images/map-layer@2x.jpg" width="1442" height="1004" class="img-xl">
            <img src="images/map-layer-md.jpg" alt="Планета" srcset="images/map-layer-md@2x.jpg" width="900" height="627" class="img-md">
        </div>
        <div class="main-wrap__content">
            <?php include "parts/header-layout.php"; ?>

            <div class="contacts-block">
                <div class="container">
                    <div class="contacts">
                        <p class="title text-heading-2">Контакты</p>

                        <div class="contacts-box">
                            <div class="contacts-row text-heading-4">
                                <div class="contacts-row__label">Телефон:</div>
                                <div class="contacts-row__value"><a href="tel:+79129283491">+7 912 928 3491</a></div>
                            </div>
                            <div class="contacts-row text-heading-4">
                                <div class="contacts-row__label">Email:</div>
                                <div class="contacts-row__value"><a href="mailto:info@nsalab.org">info@nsalab.org</a></div>
                            </div>
                            <div class="contacts-row text-heading-4">
                                <div class="contacts-row__label">Адрес:</div>
                                <div class="contacts-row__value">625000, г. Тюмень, ул.&nbsp;Республики 65, оф. 1107</div>
                            </div>
                        </div>
                        
                        <div class="contacts-social-wrap social-wrap">
                            <ul class="social-box">
                                <li class="social-item">
                                    <a href="#" class="social-unit">
                                        <svg width="30" height="30">
                                            <use xlink:href="images/icon-collections/social-icons.svg#vk-icon"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li class="social-item">
                                    <a href="#" class="social-unit" >
                                        <svg width="30" height="30">
                                            <use xlink:href="images/icon-collections/social-icons.svg#telegram-icon"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li class="social-item">
                                    <a href="#" class="social-unit">
                                        <svg width="30" height="30">
                                            <use xlink:href="images/icon-collections/social-icons.svg#facebook-icon"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li class="social-item">
                                    <a href="#" class="social-unit">
                                        <svg width="30" height="30">
                                            <use xlink:href="images/icon-collections/social-icons.svg#facebook-messenger-icon"></use>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
            </div>




        </div>

        <?php include "parts/footer-layout.php"; ?>
    </div>


    <?php include "parts/footer.php";?>
</body>
</html>
