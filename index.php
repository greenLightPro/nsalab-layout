<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Nsalab</title>
</head>
<body>
<style>
    .list-group-links li a {font-size: 17px; color: #343A40;}
</style>

<div class="container py-4">
    <h2 class="mb-4">Навигация по сайту Nsalab:</h2>
    <ul class="list-group list-group-links">
        <li class="list-group-item"><a href="main" target="_blank">Главная</a></li>
        <li class="list-group-item"><a href="projects" target="_blank">Выполненные проекты</a></li>
        <li class="list-group-item"><a href="solutions" target="_blank">Готовые решения</a></li>
        <li class="list-group-item"><a href="about" target="_blank">О нас</a></li>
        <li class="list-group-item"><a href="contacts" target="_blank">Контакты</a></li>
        <li class="list-group-item"><a href="project-page" target="_blank">Контентная страница (Проекты)</a></li>
        <li class="list-group-item"><a href="solution-page" target="_blank">Контентная страница (Решения)</a></li>
    </ul>
</div>

</body>
</html>
