<?php header("Content-Type: text/html; charset=utf-8");?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <?php include "parts/header.php";?>
</head>
<body>
<div class="main-wrap">
    <div class="main-wrap__content">
        <?php include "parts/header-layout.php"; ?>
        <section class="projects-block">
            <div class="container">
                <div class="projects">
                    <p class="text-heading-2 title">Готовые решения, доказавшие свою эффективность </p>
                    <div class="tags-wrap">
                        <ul class="tags-box">
                            <li class="tag-item"><a href="#" class="tag-unit tag-active">Все</a></li>
                            <li class="tag-item"><a href="#" class="tag-unit">Инфрафструктура</a></li>
                            <li class="tag-item"><a href="#" class="tag-unit">Облачные решения</a></li>
                        </ul>
                    </div>

                    <div class="projects-wrap">
                        <div class="projects-box grid-box ">
                            <div class="projects-box__item grid-item">
                                <div class="projects-unit fill-dark">
                                    <div class="projects-unit__cover projects-unit__cover_fill">
                                        <a href="#" class="projects-unit-cover-link"></a>
                                        <img src="images/project-preview-4.jpg"
                                             alt="Контрольно-измерительный программный комплекс"
                                             srcset="images/project-preview-4@2x.jpg 2x" width="368" height="240">
                                    </div>
                                    <div class="projects-unit__content">
                                        <p class="projects-unit-title text-accent-1">CLOUD 53</p>
                                        <p class="projects-desc text-2">Контрольно-измерительный программный комплекс</p>
                                    </div>
                                    <div class="projects-unit__footer">
                                        <a href="#" class="btn-link">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                            <div class="projects-box__item grid-item">
                                <div class="projects-unit fill-dark">
                                    <div class="projects-unit__cover projects-unit__cover_fill">
                                        <a href="#" class="projects-unit-cover-link"></a>
                                        <img src="images/project-preview-4.jpg"
                                             alt="Контрольно-измерительный программный комплекс"
                                             srcset="images/project-preview-4@2x.jpg 2x" width="368" height="240">
                                    </div>
                                    <div class="projects-unit__content">
                                        <p class="projects-unit-title text-accent-1">CLOUD 53</p>
                                        <p class="projects-desc text-2">Контрольно-измерительный программный комплекс</p>
                                    </div>
                                    <div class="projects-unit__footer">
                                        <a href="#" class="btn-link">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </div>

    <?php include "parts/footer-layout.php"; ?>
</div>


<?php include "parts/footer.php";?>
</body>
</html>

