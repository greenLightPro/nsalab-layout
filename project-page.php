<?php header("Content-Type: text/html; charset=utf-8");?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <?php include "parts/header.php";?>
</head>
<body>
    <div class="main-wrap">
        <div class="main-wrap__content">
            <?php include "parts/header-layout.php"; ?>
            <section class="page-content-block">
                <div class="container">
                    <div class="back-link-wrap">
                        <a class="back-link" href="#">
                            <svg width="40" height="40">
                                <use xlink:href="images/icon-collections/other-icons.svg#back-arrow-icon"></use>
                            </svg>
                        </a>
                    </div>
                    <div class="page-content">
                        <div class="short-container">
                            <h1 class="text-heading-2 page-content-title">Тестирование облачных инфраструктур</h1>

                            <div class="page-content-data-box">
                                <div class="page-content-data-item text-1">
                                    <div class="page-content-data-label">Период:</div>
                                    <div class="page-content-data-value">2017&nbsp;- 2019</div>
                                </div>
                                <div class="page-content-data-item text-1">
                                    <div class="page-content-data-label">Организация:</div>
                                    <div class="page-content-data-value">Тюменский государственный университет</div>
                                </div>
                            </div>

                            <div class="uni-text-content">
                                <p>По инициативе правительства Тюменской области в декабре 2016 года Тюменскому
                                    государственному унивреситету было поручено организовать на своей базе структурное
                                    подразделение с целью создания необходимых условий для подготовки молодых профессионалов
                                    к чемпионатам по стандартам WorldSkills.</p>

                                <img src="images/article-image.jpg" alt="image" srcset="images/article-image@2x.jpg 2x">

                                <p>В апреле 2017 года совместными усилиями ТюмГУ и команды NSALAB была открыта
                                    "Лаборатория сетевого и системного администрирования", которая стала базой для
                                    подготовки национальной сборной WorldSkills Russia к международным чемпионатом
                                    профмастрества и удаленной площадкой для проведения национальных чемпионатов
                                    многих стран-участниц WorldSkills International.</p>

                                <p>Инфраструктура виртуализации спроектирована на базе кластера из 14 серверов
                                    Cisco UCS 220 M4, системы хранения Dell SCv3020 и фабрики коммутаторов Cisco Nexus
                                    3524x. На базе данной инфраструктуры реализована концепция Software Defined Datacenter
                                    на базе гипервизоров VMware vSphere, гиперконвергентного хранения VMware vSAN
                                    и программно-определяемой сети на средствами VMware NSX-V.</p>

                                <p>Лабораторная инфраструктура для отработки практических навыков по сетевым
                                    технологиям состояла из маршрутизаторов, коммутаторов, межсетевых экранов,
                                    телефонов и точек беспроводного доступа производства Cisco Systems, а также консольных
                                    серверов Digi и управляемых блоков распределения питания производства APC для
                                    внеполосного контроля над оборудованием.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>

        <?php include "parts/footer-layout.php"; ?>
    </div>


    <?php include "parts/footer.php";?>
</body>
</html>
